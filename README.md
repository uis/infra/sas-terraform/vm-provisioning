# VM provisioning

This repository contains the Servers and Storage team Terrform scripts used to provision VMs in vSphere.

## How to create VMs
1. Create/choose a folder in which you'd like to define your VMs. The folder name should indicate what applications will be running on the VMs or the team that the VMs belong to. In case the folder didn't exist, please add a `README` to tell what VMs will be created in.
2. In that folder, create the Terraform script `main.tf` and define your resources (VMs in this case). You usually need to call [vSphere VM provisoning](https://gitlab.developers.cam.ac.uk/uis/infra/sas-terraform/terraform-modules/vsphere-linux) Terraform module.For example, see [./uis/services/app1](./uis/services/app1).
3. Run `tf.sh init` in the top-level directory of the repository to initialise the Terraform backend. The backend config template file `backend.tpl` will be updated with the environment variables and copied to your folder so Terraform will use it. For the moment, only Terraform [Postgres backend](https://www.terraform.io/docs/backends/types/pg.html) is supported. Other template files will also be copied each time you run a `tf.sh` command.

4. Run `tf.sh plan/apply` to create your VMs.

## Wrapper script
`tf.sh` is a wrapper script that runs Terraform commands on *all* the folders containing `.tf` files.

The script updates the template files `*.tpl` with environment variables and copies them to the Terraform module directories. Then, it runs the Terraform command passed to it as arguments. 

Help message of `tf.sh`:
```bash
$ ./tf.sh -h
Usage: ./tf.sh [options] CMD1 CMD2 ...

A simple wrapper script that runs terraform commands on multiple folders.

OPTIONS:
-h                                  Show this help message
-d dir                              Optional. The dir in which to run the terraform command.
                                    This will be passed as [dir] option to Terraform.
                                    If not specified, run the Terraform commands on all
                                    the sub-directories.

E.g.
Run terraform plan in ./examples/test: ./tf.sh -d ./examples/test plan
Run terraform init on all the folders containing .tf files: ./tf.sh init
```

## Postgres backend
For this project, the postgres terraform backend is running on `tf-live-pg1.srv.uis.private.cam.ac.uk`.
