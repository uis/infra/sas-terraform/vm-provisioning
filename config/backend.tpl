# pg backend
terraform {
  backend "pg" {
    conn_str    = "postgres:///"
    schema_name = "$PG_SCHEMA" # this should be specific to each terraform directory
  }
}
