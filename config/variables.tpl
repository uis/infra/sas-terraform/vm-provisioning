variable "vsphere_user" {
  description = "vSphere user name"
}

variable "vsphere_password" {
  description = "vSphere password"
  sensitive   = true
}

variable "vsphere_server" {
  description = "vCenter server FQDN or IP"
  default     = "vcenter.srv.uis.private.cam.ac.uk"
}

variable "vsphere_unverified_ssl" {
  description = "Is the vCenter using a self signed certificate (true/false)"
  default     = false
}
