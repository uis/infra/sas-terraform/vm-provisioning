locals {
  moa_nonprod_folder = "/Servers and Storage/Moa/Moa non-production clusters"
}

module "moa-dev-patroni1" {
  source       = "./moa-vm-v1"
  hostname     = "moa-dev-patroni1"
  folder       = "${local.moa_nonprod_folder}/moa-dev-patroni"
  db_disk_size = 100
}

module "moa-dev-patroni2" {
  source       = "./moa-vm-v1"
  hostname     = "moa-dev-patroni2"
  folder       = "${local.moa_nonprod_folder}/moa-dev-patroni"
  db_disk_size = 100
}

module "moa-dev-patroni3" {
  source       = "./moa-vm-v1"
  hostname     = "moa-dev-patroni3"
  folder       = "${local.moa_nonprod_folder}/moa-dev-patroni"
  db_disk_size = 100
}

module "map-dev-db1" {
  source       = "./moa-vm-v1"
  hostname     = "map-dev-db1"
  folder       = "${local.moa_nonprod_folder}/map-dev-db"
}

module "map-dev-db2" {
  source       = "./moa-vm-v1"
  hostname     = "map-dev-db2"
  folder       = "${local.moa_nonprod_folder}/map-dev-db"
}

module "map-dev-db3" {
  source       = "./moa-vm-v1"
  hostname     = "map-dev-db3"
  folder       = "${local.moa_nonprod_folder}/map-dev-db"
}
