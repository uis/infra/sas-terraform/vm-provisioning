# Module for Moa nodes on the legacy VM farm.

# Input Variables.

variable "hostname" {
  type     = string
  nullable = false
}
variable "location" {
  type    = string
  default = null
}
variable "folder" {
  type     = string
  nullable = false
}
# Optional settings with reasonable defaults.
variable "db_disk_size" {
  type     = number
  default  = 10 # Expressed in GiB
  nullable = false
}
variable "num_cpus" {
  type     = number
  default  = 1
  nullable = false
}
variable "memory" {
  type     = number
  default  = 2048 # Enough for Tanium.
  nullable = false
}
variable "network" {
  default = null
}
variable "backup_status" {
  type    = string
  default = null
}

# Data Items (things that exist already).

data "vsphere_datacenter" "datacenter" {
  name = "UIS"
}

data "vsphere_custom_attribute" "backup_status" {
  name = "Backup Status:"
}

data "vsphere_compute_cluster" "wcdc" {
  name          = "WCDC"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_datastore_cluster" "wcdc" {
  name          = "WCDC_Pure_VM_vols"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_compute_cluster" "sby" {
  name          = "SBY"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_datastore_cluster" "sby" {
  name          = "SBY_Pure_VM_vols"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_compute_cluster" "cnh" {
  name          = "CNH"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_datastore" "cnh" {
  name          = "CNH_vSAN"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_network" "vlan_0808" {
  name          = "0808 cs-srv-8 (UCS Main)"
  datacenter_id = data.vsphere_datacenter.datacenter.id
}

data "vsphere_content_library" "library" {
  name = "Library Two"
}

data "vsphere_content_library_item" "ubuntu_2404" {
  name       = "ubuntu-24.04-server-cloudimg-amd64"
  type       = "ovf"
  library_id = data.vsphere_content_library.library.id
}

locals {
  ssh_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGEaTNxZDg3xzWWyQXR1CxAJoTtCjHZu5Cc8VR6Hcxbd bjh21@cam.ac.uk (gnarl admin key)"
  user_data = {
    keyboard     = { layout = "gb" }
    timezone     = "Europe/London"
    users        = []
    disable_root = false
    disk_setup = {
      "/dev/sdb" = {
        table_type = "gpt"
        # layout=true means create a single large partition.
        layout = true
      }
    }
    fs_setup = [{
      device     = "/dev/sdb1"
      filesystem = "ext4"
    }]
    mounts = [["/dev/sdb1", "/var/lib/postgresql", "ext4"]]
    swap = {
      filename = "/var/swapfile"
      size     = "1G"
    }
  }
  locations = {
    wcdc = {
      resource_pool_id     = data.vsphere_compute_cluster.wcdc.resource_pool_id
      datastore_id         = null
      datastore_cluster_id = data.vsphere_datastore_cluster.wcdc.id
    }
    sby = {
      resource_pool_id     = data.vsphere_compute_cluster.sby.resource_pool_id
      datastore_id         = null
      datastore_cluster_id = data.vsphere_datastore_cluster.sby.id
    }
    cnh = {
      resource_pool_id     = data.vsphere_compute_cluster.cnh.resource_pool_id
      datastore_id         = data.vsphere_datastore.cnh.id
      datastore_cluster_id = null
    }
  }
  # Default location based on numeric part of hostname.
  location = coalesce(var.location,
  element(["cnh", "wcdc", "sby"], regex("\\d+$", var.hostname)))
  # Default backup status similarly.
  backup_status = coalesce(var.backup_status,
  length(regexall("\\D1$", var.hostname)) > 0 ? "Oook" : "Not Required")
}

resource "vsphere_virtual_machine" "vm" {
  name                 = var.hostname
  resource_pool_id     = local.locations[local.location].resource_pool_id
  datastore_id         = local.locations[local.location].datastore_id
  datastore_cluster_id = local.locations[local.location].datastore_cluster_id
  folder               = var.folder
  hardware_version     = 19

  num_cpus           = var.num_cpus
  memory             = var.memory
  memory_reservation = var.memory

  network_interface {
    network_id = coalesce(var.network, data.vsphere_network.vlan_0808).id
  }

  disk {
    label            = "disk0"
    thin_provisioned = true
    size             = 10
  }
  disk {
    label            = "disk1"
    thin_provisioned = true
    size             = var.db_disk_size
    unit_number      = 1
  }

  clone {
    # See <https://stackoverflow.com/questions/72908131>.
    template_uuid = data.vsphere_content_library_item.ubuntu_2404.id
  }

  # Required for vApp properties.
  cdrom {
    client_device = true
  }

  vapp {
    properties = {
      hostname    = var.hostname
      instance-id = var.hostname
      public-keys = local.ssh_key
      user-data = base64encode(<<-EOF
        #cloud-config
        ${jsonencode(local.user_data)}
	EOF
      )
    }
  }

  custom_attributes = {
    (data.vsphere_custom_attribute.backup_status.id) = local.backup_status
  }

  wait_for_guest_net_timeout = 0
  wait_for_guest_ip_timeout  = 0

  lifecycle {
    # vApp properties are only used at initial installation, so they can
    # be ignored thereafter.  Without this, Terraform reboots all VMs on
    # every change to user_data, which is unhelpful.
    ignore_changes = [vapp]
  }
}
