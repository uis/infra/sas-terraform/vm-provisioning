provider "aws" {
  region = "eu-west-2"
  default_tags {
    tags = {
      managed_by_terraform = "infra-sas/moa"
    }
  }
}

### VPC components
# These are responsible for networking to our VMs.

# A VPC for any containers to connect to.
resource "aws_vpc" "moa-etcd" {
  # See <https://docs.aws.amazon.com/vpc/latest/userguide/working-with-vpcs.html>
  tags = {
    Name = "moa-etcd"
  }

  # All VPCs need an IPv4 block.
  cidr_block                       = "10.55.18.0/24"
  assign_generated_ipv6_cidr_block = true
  # DNS hostnames may be necessary to use EFS from ECS, because otherwise
  # the EFS mount target doesn't get a name.
  enable_dns_hostnames = true
}

# And a subnet within that VPC.
resource "aws_subnet" "moa-etcd" {
  vpc_id = aws_vpc.moa-etcd.id
  tags = {
    Name = "moa-etcd"
  }

  # ECS tasks deployed on Fargate need an IPv4 address, so the subnet
  # needs an IPv4 block.
  cidr_block      = cidrsubnet(aws_vpc.moa-etcd.cidr_block, 2, 0)
  ipv6_cidr_block = cidrsubnet(aws_vpc.moa-etcd.ipv6_cidr_block, 8, 0)

  assign_ipv6_address_on_creation                = true
  enable_resource_name_dns_aaaa_record_on_launch = true
}

# And an Internet gateway.
resource "aws_internet_gateway" "moa-etcd" {
  vpc_id = aws_vpc.moa-etcd.id

  tags = {
    Name = "moa-etcd"
  }
}

# A route table for the subnet.
resource "aws_route_table" "moa-etcd" {
  vpc_id = aws_vpc.moa-etcd.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.moa-etcd.id
  }
  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.moa-etcd.id
  }
}
resource "aws_route_table_association" "moa-etcd" {
  subnet_id      = aws_subnet.moa-etcd.id
  route_table_id = aws_route_table.moa-etcd.id
}

# Everywhere should allow ICMP as a matter of principle.
resource "aws_security_group" "allow-icmp" {
  name   = "allow-icmp"
  vpc_id = aws_vpc.moa-etcd.id
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port        = -1
    to_port          = -1
    protocol         = "icmpv6"
    ipv6_cidr_blocks = ["::/0"]
  }
}

# A security group for the service.
resource "aws_security_group" "moa-etcd" {
  name   = "moa-etcd"
  vpc_id = aws_vpc.moa-etcd.id
  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    from_port        = 2380
    to_port          = 2380
    protocol         = "tcp"
    ipv6_cidr_blocks = ["2001:630:212:8::/64"]
  }
  egress {
    protocol         = "-1"
    from_port        = 0
    to_port          = 0
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

###
# Virtual machines (or "instances" in AWS-speak)

# Find the current Canonical Ubuntu 20.04 image.
# https://ubuntu.com/server/docs/cloud-images/amazon-ec2
data "aws_ssm_parameter" "ubuntu_2004_ami" {
  name = "/aws/service/canonical/ubuntu/server/20.04/stable/current/amd64/hvm/ebs-gp2/ami-id"
}

data "aws_ssm_parameter" "ubuntu_2404_ami" {
  name = "/aws/service/canonical/ubuntu/server/24.04/stable/current/amd64/hvm/ebs-gp3/ami-id"
}

# Initial root SSH key.
resource "aws_key_pair" "bjh21_id_gnarl" {
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGEaTNxZDg3xzWWyQXR1CxAJoTtCjHZu5Cc8VR6Hcxbd bjh21@cam.ac.uk (gnarl admin key)"
}

# The list of VMs to create.
locals {
  vms = {
    # moa-dev-patronietcd1        = { ipv6_suffix = parseint("00db09fe", 16) }
    # moa-rwhb2sample-etcd1       = { ipv6_suffix = parseint("00db09de", 16) }
    # irs-dev-dbetcd1             = { ipv6_suffix = parseint("00db09ce", 16) }
    # irs-live-dbetcd1            = { ipv6_suffix = parseint("00db096e", 16) }
    # cert-blackhole-dbetcd1      = { ipv6_suffix = parseint("00db09be", 16) }
    # map-dev-dbetcd1             = { ipv6_suffix = parseint("00db099e", 16) }
    # map-live-dbetcd1            = { ipv6_suffix = parseint("00db09ae", 16) }
    # tel-live-dbetcd1            = { ipv6_suffix = parseint("00db098e", 16) }
    # vpn-dbetcd1                 = { ipv6_suffix = parseint("00db097e", 16) }
    # devgroup-test-dbetcd1       = { ipv6_suffix = parseint("00db095e", 16) }
    # netsys-mgt-dbetcd1          = { ipv6_suffix = parseint("00db09ee", 16) }
    # utbs-live-dbetcd1           = { ipv6_suffix = parseint("00db094e", 16) }
    # lookup-live-dbetcd1         = { ipv6_suffix = parseint("00db093e", 16) }
    # passwordapp-live-dbetcd1    = { ipv6_suffix = parseint("00db092e", 16) }
    # sms-live-dbetcd1            = { ipv6_suffix = parseint("00db091e", 16) }
    # hta-live-dbetcd1            = { ipv6_suffix = parseint("00db090e", 16) }
    # ppsw-dbetcd1                = { ipv6_suffix = parseint("00db08ee", 16) }
    # wireless-dbetcd1            = { ipv6_suffix = parseint("00db08de", 16) }
    # stufunding-prod-dbetcd1     = { ipv6_suffix = parseint("00db08ce", 16) }
    # stufunding-nonprod-dbetcd1  = { ipv6_suffix = parseint("00db08be", 16) }
    # infra-dbetcd1               = { ipv6_suffix = parseint("00db088e", 16) }
    # network-tokens-live-dbetcd1 = { ipv6_suffix = parseint("00db087e", 16) }
    # poole-hta-live-dbetcd1      = { ipv6_suffix = parseint("00db086e", 16) }
    # zabbix-dbetcd1              = { ipv6_suffix = parseint("00db085e", 16) }
  }
  user_data = {
    keyboard     = { layout = "gb" }
    timezone     = "Europe/London"
    users        = []
    disable_root = false
  }
}

# Make a VM.
resource "aws_instance" "moa-etcd" {
  ami           = data.aws_ssm_parameter.ubuntu_2004_ami.value
  instance_type = "t3a.nano"
  key_name      = aws_key_pair.bjh21_id_gnarl.key_name
  for_each      = local.vms

  subnet_id                   = aws_subnet.moa-etcd.id
  associate_public_ip_address = false
  ipv6_addresses = [
    cidrhost(aws_subnet.moa-etcd.ipv6_cidr_block, each.value.ipv6_suffix)
  ]
  vpc_security_group_ids = [
    aws_security_group.allow-icmp.id,
    aws_security_group.moa-etcd.id
  ]
  root_block_device {
    volume_type = "gp3"
  }
  tags = {
    Name = each.key
  }
  user_data = <<-EOF
        #cloud-config
        ${jsonencode(local.user_data)}
	EOF
}

moved {
  from = aws_instance.moa-dev-patronietcd1
  to   = aws_instance.moa-etcd["moa-dev-patronietcd1"]
}
